# Unilever Qualifio Tracking Library

- [Unilever Qualifio Tracking Library](#unilever-qualifio-tracking-library)
  - [Introduction](#introduction)
  - [Process of implementing tracking & measurement](#process-of-implementing-tracking--measurement)
    - [table of content](#table-of-content)
  - [1. Setting up a tag manager](#1-setting-up-a-tag-manager)
    - [Setting / getting up a tag manager code](#setting--getting-up-a-tag-manager-code)
    - [Importing a tag managment container](#importing-a-tag-managment-container)
  - [2. Consent script](#2-consent-script)
  - [3. Qualifio scripting](#3-qualifio-scripting)
    - [Base Code](#base-code)
    - [Different Elements](#different-elements)
      - [Tagging Plan Code](#tagging-plan-code)
      - [Initial Settings](#initial-settings)
      - [Pageview Initialization](#pageview-initialization)
      - [Scores & Rules](#scores--rules)
      - [Qualifio content interactions](#qualifio-content-interactions)
  - [Tag Management Container](#tag-management-container)
    - [Tags](#tags)
    - [Variables](#variables)
    - [Triggers](#triggers)
    - [Custom Templates](#custom-templates)
  - [Testing your setup](#testing-your-setup)
    - [1. OneTrust is implemented](#1-onetrust-is-implemented)
    - [2. Google Tag Manager is implemented](#2-google-tag-manager-is-implemented)
    - [3. Is a dataLayer available](#3-is-a-datalayer-available)
    - [3. Scripts are loading](#3-scripts-are-loading)

## Introduction

Unilever is making use of Qualifio for fast / flexible campaign deployment.

We're using the tool for
* Informing our audience
* Generating leads
* Building profiles

Out of the box qualifio is hosttin pieces of analytics like
* Participant data
* Who's visiting content from where

To enhance our capabilities of tracking & segmenting on top of qualifio we're implementing:
1. Consent Management => OneTrust
2. Tag Management => Google Tag Management
3. Webanalytics => Google Analytics
4. Marketing scripts => [facebook, google]

This guide will take through setting up, implementing & maintaining Qualifio related projects.  
<br>

***

## Process of implementing tracking & measurement

There are a couple of step to take care of when implementing scripts & tags.

### table of content 

1. Setting up a tag manager code or getting a existing container *[google tag manager]*
2. Importing a container from this repository *[ gitlab / google tag manager ]*
3. Implementing consent managment *[ qualifio ]*
4. Implementing consent managment *[ qualifio ]*  
5. Implementing a layer of data *[ qualifio ]*
6. Configure settings of marketing tags *[ google tag manager ]* 
7. Perform testing on:
   1. consent management
   2. datalayer
   3. google analytics

*** 

## 1. Setting up a tag manager

### Setting / getting up a tag manager code

<details>
<br>


A first step is to either create or reuse a **Google Tag Manager Script**. 
To centralize this, we're making use of this tag manager account:   
**Account**: Unilever NL | Qualifio Minisites 
**Account ID**: 6001832741
<br>  
When access is missing you can request access to **someone@unilever.com**

*Tag manager Container*
![tag manager account](images/tagmanager/tagmanageraccount.png "tag manager account")

To create a new container:
1. Access one of the containers
2. Open the Admin space
3. Pick the "Plus" sign

*Add a new container*
![Add new container](images/tagmanager/add_new_container.png "Add new container")

*Select web and fill in a descriptive name*
![Dove | minisites](images/tagmanager/dove_ministes_example.png "Dove | minisites")

*From the container admin screen now select the option install tag manager*
![Install Tag Manager](images/tagmanager/install_tag_manager.png "Install Tag Manager")

*Get the script code to install & ignore the no script option*
![Copy Tag Manager Code](images/tagmanager/copy_tag_manager_code.png "Copy Tag Manager Code")

</details>

***

### Importing a tag managment container

<details>
<br>

The best way to set up a tag manager is to import our **standard_qualifio_workspace_package**. You can download the json file form the scripts section of this repository.

For importing the workspace you need to follow these instructions:
1. Access the containers Admin section
2. Select the inport container functionality
3. Select the container file
4. Create a new workspace
5. Select overwrite (! overwriting is overwriting other container content / completely replacing content)
6. Accept items items to start the import process

**Visual details**
   <details>
   <br>


   *Import a Tag Manager Workspace*
   ![Access the admin area and pick import container](images/tagmanager/import_workspace_from_tag_manager.png "Access the admin area and pick import container")

   *Select the workspace package, set up a new workspace & select overwrite*
   ![Select the workspace package, set up a new workspace & select overwrite](images/tagmanager/preview_and_review.png "Select the workspace package, set up a new workspace & select overwrite")

   *Preview & Review container*
   ![Preview & Review container](images/tagmanager/standard_qualifio_workspace_package.png "Preview & Review container")

   </details>

</details>

***
<br>
<br>

## 2. Consent script

<details>
<br>


The next action is to implement the consent management script of OneTrust & implement the Google Tag Manager container in the same Qualifio settings screen. Therefore we need to take these steps:  

1. Open the Qualifio Campaign Manager
2. Look for the campaign where you want to add the scripts (when your campaign is spanning multiple pages, add the code to all these instance seperately)
3. Select the option for editing the campaign
4. Go to look and feel
5. Select the template and pick the option that you want to customize it
6. Open the Advanced settings box
7. Copy paste your code from the gitlab repository
8. Put the code into the HTML section
9. Apply the HTML and select Save & Quit

**Visual details**
   <details>
   <br>


   *Open up Qualifio and search for the campaign where you want to implement scripts*
   ![Open Qualifio and select campaign](images/qualifio/open_campaign_you_need_to_tag.png "Open Qualifio and select campaign")

   *Edit the campaign and open up look and feel / visualization, select the template and pick the option for customizing*
   ![Edit the Campaign](images/qualifio/go_to_look_and_feel_select_template_and_pick_customize.png "Edit the Campaign")

   *Open the advanced settings, select the HTML, put in the consent manager code on to and the tag manager below, and apply the HTML before saving*
   ![Apply code in settings](images/qualifio/go_to_advanced_settings_get_the_tag_manager_consent_codes_implemented.png "Apply code in settings")

   *Save up & quit*
   ![Save & Quit](images/qualifio/save_the_template_and_quit.png "Save & Quit")

   </details>

</details>

***
<br>
<br>

## 3. Qualifio scripting

### Base Code

<details>
<br>


In this section we start to implement the Qualifio Tagging plan. Therefor you need to follow these steps:
1. Open up the Qualifio overview page again
2. Select the campaign you want tagging to be implemented for
3. Edit the campaign
4. Open the section for tags throught first dot Channels
5. Copy the Tag Plan from the repository in Gitlab
6. Place the code in tracking tags and pixels
7. Save & quit

**Visual details**
   <details>
   <br>

   
   *Open the Qualifio Page again*
   ![Open the Qualifio page again](images/qualifio/open_campaign_you_need_to_tag.png "Open the Qualifio page again")

   *Open the Tags section and the tab for tracking tags and pixels to apply the code, save & quit*
   ![Insert code in tags](images/qualifio/insert_code_in_tags.png "Insert code in tags")

   </details>

</details>

***

### Different Elements
The Tagging plan is important to us as it functions as a blueprint that we are using across all the Qualifio implementations. It allows us to set up basic information to parse to a Google Tag Manager container. This information can then be used to triggers / fire & fuel script & tag activation, allowing us to:
1. Track behavior
2. Measure success
3. Create new audience data

<details>

#### Tagging Plan Code
   <details>
   **Important**: 
   Qualifio can both be used in a iframed & stand-alone setup. Both use a different code setup that can be found in the 
   ** 'qualifio_tagging_plan.js'** document.

    *Standalone*: making use of: *_qual_async.push(['requestPlugin', 'qualp', 'datalayer', function()*. 
    This method is parsing the input to the parentpage (a post message like technology).

    *Iframed*: works with the standard dataLayer.push functionality.

   <br>


```javascript

   <script>
   $(function() {
      
      /**
       * set qualifio generic script
       * push to the parent page
       */
      
      var  tp = _qlf_taggingplan,
      _brandName = '', // fill in the brand name or abreviation
      _ulCampaignId = ''; // fill in the campaign id
      
      /* get qualifio datalayer */
      qlfd = qlfDataLayer[0]
      
      /* set dataLayer object */
      dataLayerPageView = {
         'campaign_brand': _brandName,
         'unilever_campaign_id': _ulCampaignId,
         'page_path': qlfd['page_path'] + '?{utms}',
         'title': qlfd['title'],
         'campaign_id': qlfd['campaign_id'],
         'campaign_guid': qlfd['campaign_guid'],
         'channel_guid': qlfd['channel_guid'],
         'device': qlfd['device'],
         'language': qlfd['lg'],
         'page': qlfd['pg'],
         'pgi': qlfd['pgi'],
         'domain': qlfd['domain'],
         'step': qlfd['step'],
         'winner': qlfd['isWinner'],
         'score': qlfd['score'],
         'location': qlfd['parentURL'] + qlfd['page_path'] + '?{utms}',
         'utm_parameters': '{utms}',
         'event': 'virtualPageview'
      };
      /**
       * Virtual Pageview
       */
      _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
         _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', dataLayerPageView]);
      }]);
      
      /** Scores*/
      $('#s_scores').click(function(){
         _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
               _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                  'event':'gaEvent',
                  'eventCategory':'Click',
                  'eventAction': 'Rules',
                  'eventLabel': qlfDataLayer['0']['title']
               }]);
         }]);    
      });
      
      /** Rules */
      $('#s_reglement').click(function(){
         _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
               _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                  'event':'gaEvent',
                  'eventCategory':'Click',
                  'eventAction': 'Scores',
                  'eventLabel': qlfDataLayer['0']['title']
               }]);
         }]);    
      });
      
      
      /**
       * Play the game
       */
      if (tp.getStepName() == 'intro') {
         // Play button
         $('#jouer').click(function(){
               _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                  _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                     'event':'gaEvent',
                     'eventCategory':'Click',
                     'eventAction': 'Participate',
                     'eventLabel': qlfDataLayer['0']['title']
                  }]);
               }]);    
         });
      }
      /**
       * Question set
       */
      if (tp.getStepName().startsWith("questionset")) {
         /** next */
         $('#suivant1, #next').click(function(){
               _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                  _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                     'event':'gaEvent',
                     'eventCategory':'Question ' + qlfDataLayer['0']['pgi'],
                     'eventAction': 'Answered',
                     'eventLabel': qlfDataLayer['0']['title']
                  }]);
               }]); 
               
         });
         /** Images */
         $('.answerPic').click(function() {
               _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                  _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                     'event':'gaEvent',
                     'eventCategory':'Question ' + qlfDataLayer['0']['pgi'],
                     'eventAction': 'Answered',
                     'eventLabel': qlfDataLayer['0']['title']
                  }]);
               }]); 
         });
         /** Answering */
         $('[id^=check_choix]').click(function() {
               _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                  _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                     'event':'gaEvent',
                     'eventCategory':'Question ' + qlfDataLayer['0']['pgi'],
                     'eventAction': 'Answered',
                     'eventLabel': qlfDataLayer['0']['title']
                     
                  }]);
               }]); 
         });
      }
      if (tp.isFormSubmittedRightNow()) { 
         /** form submit data */    
               _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                  _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                     'event':'gaEvent',
                     'eventCategory':'Form',
                     'eventAction': 'Submit',
                     'eventLabel': qlfDataLayer['0']['title']
                  }]);
               }]);
      }
      if (tp.getStepName().startsWith("exit")) { 
         /** winner data */
         if (tp.isWinner() == 1) { 
               _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                  _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                     'event': 'gaEvent',
                     'eventCategory': 'Click',
                     'eventAction': 'Quiz finished - Won',
                     'eventLabel': qlfDataLayer['0']['title']
                  }]);
               }]);   
         }
         if (tp.isWinner() == 0) { 
               _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                  _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                     'event': 'gaEvent',
                     'eventCategory': 'Click',
                     'eventAction': 'Quiz finished - Lost',
                     'eventLabel': qlfDataLayer['0']['title']
                  }]);
               }]);  
         }
         /** share facebookj */
         $('#fbShareWall a').click(function() {
               if (tp.isAShareFor(this, "facebook")) {
                  _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                     _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                           'event': 'gaEvent',
                           'eventCategory': 'Click',
                           'eventAction': 'Share participation - Facebook',
                           'eventLabel': qlfDataLayer['0']['title']
                     }]);
                  }]);  
                  
               }
         });
         /** share twitter */
         $('#fbShareWall a').click(function() {
               if (tp.isAShareFor(this, "twitter")) {
                  _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                     _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                           'event': 'gaEvent',
                           'eventCategory': 'Click',
                           'eventAction': 'Share participation - Twitter',
                           'eventLabel': qlfDataLayer['0']['title']
                     }]);
                  }]);  
                  
               }
         });
         /** replay the game */
         $('#fbShareWallBonus > a.bouton').click(function() {
               _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                  _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                     'event': 'gaEvent',
                     'eventCategory': 'Click',
                     'eventAction': 'Replay',
                  }]);
               }]);  
         });
      }
   });
   </script>
   ```
   </details>

   ***
   
   #### Initial Settings
   Setting up the initial details of the tagging plan. Containing all the basic info & settings attributed to a campaign  

   <details>
   <br>


   ```javascript
   
   var  tp = _qlf_taggingplan,
      _brandName = '', // fill in the brand name or abreviation
      _ulCampaignId = ''; // fill in the campaign id
      
      /* get qualifio datalayer */
      qlfd = qlfDataLayer[0]
      
      /* set dataLayer object */
      dataLayerPageView = {
         'campaign_brand': _brandName,
         'unilever_campaign_id': _ulCampaignId,
         'page_path': qlfd['page_path'] + '?{utms}',
         'title': qlfd['title'],
         'campaign_id': qlfd['campaign_id'],
         'campaign_guid': qlfd['campaign_guid'],
         'channel_guid': qlfd['channel_guid'],
         'device': qlfd['device'],
         'language': qlfd['lg'],
         'page': qlfd['pg'],
         'step': qlfd['step'],
         'device': qlfd['device'],
         'pgi': qlfd['pgi'],
         'domain': qlfd['domain'],
         'step': qlfd['step'],
         'winner': qlfd['isWinner'],
         'score': qlfd['score'],
         'location': qlfd['parentURL'] + qlfd['page_path'] + '?{utms}',
         'utm_parameters': '{utms}',
         'event': 'virtualPageview'
      };

   ```
   </details>

   ***

   #### Pageview Initialization 
   A pageview is being generated each time the qualifio instance is loading (virtual pageview)  

   <details>
   <br>


   ```javascript
   
   /**
    * Virtual Pageview
    */
    _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
        _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', dataLayerPageView]);
    }]);
   ```

   </details>

   ***

   #### Scores & Rules
   Data is being activated as soon as a user interacts with rules & scores  
   
   <details>
   <br>

   
   ```javascript
   /** Scores*/
    $('#s_scores').click(function(){
        _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
            _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                'event':'gaEvent',
                'eventCategory':'Click',
                'eventAction': 'Rules',
                'eventLabel': qlfDataLayer['0']['title']
            }]);
        }]);    
    });
    
    /** Rules */
    $('#s_reglement').click(function(){
        _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
            _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                'event':'gaEvent',
                'eventCategory':'Click',
                'eventAction': 'Scores',
                'eventLabel': qlfDataLayer['0']['title']
            }]);
        }]);    
    });
   
   ```

   </details>

   ***

   #### Qualifio content interactions

   These interactions are supported: 
   
   1. Start the content / game
   2. Images, pictures & next step interactions
   3. Submit event 
   4. Winner event: win / lose
   5. Share on Twitter, Facebook & Other media
   6. Replay game / content

   <details>
   <br>


   ```javascript

   /**
    * Play the game
    */
    if (tp.getStepName() == 'intro') {
        // Play button
        $('#jouer').click(function(){
            _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                    'event':'gaEvent',
                    'eventCategory':'Click',
                    'eventAction': 'Participate',
                    'eventLabel': qlfDataLayer['0']['title']
                }]);
            }]);    
        });
    }
    /**
    * Question set
    */
    if (tp.getStepName().startsWith("questionset")) {
        /** next */
        $('#suivant1, #next').click(function(){
            _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                    'event':'gaEvent',
                    'eventCategory':'Question ' + qlfDataLayer['0']['pgi'],
                    'eventAction': 'Answered',
                    'eventLabel': qlfDataLayer['0']['title']
                }]);
            }]); 
            
        });
        /** Images */
        $('.answerPic').click(function() {
            _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                    'event':'gaEvent',
                    'eventCategory':'Question ' + qlfDataLayer['0']['pgi'],
                    'eventAction': 'Answered',
                    'eventLabel': qlfDataLayer['0']['title']
                }]);
            }]); 
        });
        /** Answering */
        $('[id^=check_choix]').click(function() {
            _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                    'event':'gaEvent',
                    'eventCategory':'Question ' + qlfDataLayer['0']['pgi'],
                    'eventAction': 'Answered',
                    'eventLabel': qlfDataLayer['0']['title']
                    
                }]);
            }]); 
        });
    }
    if (tp.isFormSubmittedRightNow()) { 
        /** form submit data */    
            _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                    'event':'gaEvent',
                    'eventCategory':'Form',
                    'eventAction': 'Submit',
                    'eventLabel': qlfDataLayer['0']['title']
                }]);
            }]);
    }
    if (tp.getStepName().startsWith("exit")) { 
        /** winner data */
        if (tp.isWinner() == 1) { 
            _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                    'event': 'gaEvent',
                    'eventCategory': 'Click',
                    'eventAction': 'Quiz finished - Won',
                    'eventLabel': qlfDataLayer['0']['title']
                }]);
            }]);   
        }
        if (tp.isWinner() == 0) { 
            _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                    'event': 'gaEvent',
                    'eventCategory': 'Click',
                    'eventAction': 'Quiz finished - Lost',
                    'eventLabel': qlfDataLayer['0']['title']
                }]);
            }]);  
        }
        /** share facebook */
        $('#fbShareWall a').click(function() {
            if (tp.isAShareFor(this, "facebook")) {
                _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                    _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                        'event': 'gaEvent',
                        'eventCategory': 'Click',
                        'eventAction': 'Share participation - Facebook',
                        'eventLabel': qlfDataLayer['0']['title']
                    }]);
                }]);  
                
            }
        });
        /** share twitter */
        $('#fbShareWall a').click(function() {
            if (tp.isAShareFor(this, "twitter")) {
                _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                    _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                        'event': 'gaEvent',
                        'eventCategory': 'Click',
                        'eventAction': 'Share participation - Twitter',
                        'eventLabel': qlfDataLayer['0']['title']
                    }]);
                }]);  
                
            }
        });
        /** replay the game */
        $('#fbShareWallBonus > a.bouton').click(function() {
            _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                    'event': 'gaEvent',
                    'eventCategory': 'Click',
                    'eventAction': 'Replay',
                }]);
            }]);  
        });
   ```

   </details>

</details>

***

## Tag Management Container

***

### Tags
These Tags are currently supported. Tags represent bits & pieces of code and structured template allowing us to track / measure success or modify code on page etc.

These type of tags are used in the container:
1. Custom HTML (code)
2. Standard tag templates
   1. Google Analytics
   2. Consent Tags
3. Custom tag templates
   1. Advertising tags
   2. Adobe Demdex

<details>
<br>


* Google Analytics - Event - Time on site
* Google Analytics - Event - uaEvent
* Facebook - VirtualPage - Pageview
* Google Analytics - Regular Pageviews - Accept Consent
* Facebook - VirtualPage - Subscribe
* Custom HTML - Utility - Time on site
* Facebook - VirtualPage - Lead
* Facebook - Qvisit
* Google Analytics - Regular Pageviews
* Custom Image - Impression Demdex - Generic

</details>

***

### Variables
These variables are currently supported. Variables represent a dynamic way to include information. Instead of using 100 tags the use of variables can bring this down to just one as information being parsed through these variables are dynamic. Variables get the content based on:
1. cookies
2. constant values
3. html element values
4. datalayer values
5. custom javascripts
6. javascript variables

<details>
<br>


* Click Element
* Click Classes
* Click ID
* Click Target
* Click Text
* Form Element
* Form Classes
* Form ID
* Form Target
* Form URL
* Form Text
* Error Message
* Error URL
* Error Line
* New History Fragment
* Old History Fragment
* New History State
* Old History State
* History Source
* Container Version
* Debug Mode
* Random Number
* Container ID
* HTML ID
* Environment Name
* Video Provider
* Video URL
* Video Title
* Video Duration
* Video Percent
* Video Visible
* Video Status
* Video Current Time
* Scroll Depth Threshold
* Scroll Depth Units
* Scroll Direction
* Percent Visible
* On-Screen Duration
* Page URL
* Page Hostname
* Referrer
* Event
* OneTrust - Cookie - OptanonConsent
* OneTrust - Custom Javascript - Consent status
* DLV - page
* qlfDataLayer - query_string
* qlDataLayer - parentURL
* qlfDataLayer - total url
* DLV - Click - gtm.element
* DLV - score
* DLV - utm_parameters
* DLV - Location
* DLV - campaign_guid
* DLV - campaign_brand
* DLV - campaign_id
* DLV - language
* DLV - pgi
* DLV - title
* DLV - channel_guid
* DLV - step
* DLV - device
* DLV - domain
* DLV - OptanonActiveGroups
* DLV - page_path
* DLV - eventLabel
* DLV - eventCategory
* DLV - eventAction
* DLV - Click - gtm.element.href
* JSV - virtualPageCount
* DLV - Click - gtm.element.alt
* DLV - optanonAction
* DLV - Click - gtm.element.parentNode.className
* DLV - gtm.formSubmit.value
* DLV - Click - gtm.element.host
* DLV - gtm.elementUrl
* DLV - winner
* DLV - virtualReferrer
* DLV - c_brandcode
* Constant - FBIDS
* gaID_UA
* Lookup Regex Table - Registration Mapping
* Lookup Regex Table - qlic - internal links
* SessionStorage - GetStorage - VirtualPageviews
* gaSettingsVariable


</details>

***

### Triggers
These triggers are currently supported. Triggers are applied to fire / keep tags from firing.

<details>
<br>

* Custom Event - virtualPageview
* Custom Event - gaEvent
* Custom Event - Time on site
* Custom Event - gaEvent - Lead
* Custom Event - trackOptanonEvent - Accept
* Custom Event - virtualPageview - exit
* OneTrust - Allow Statistics & Advertising Cookie
* OneTrust - Block Functional Cookie
* OneTrust - Block Advertising Cookie
* OneTrust - Allow Advertising Cookie
* Form Submit - Register
* Form Submit - Launch Game
* Form Submit - Next Step
* Link Click - nav-arrow - next and previous link
* Link Click - qlic - internal qualifio links
* Link Click - _blank - external link
* Link Click - Boutons

</details>

***

### Custom Templates
These templates are created to offer marketeers a simple way of setting up advertising tags. 

<details>
<br>

* Facebook Pixel
* [ Utility ] Time on site
* [ Marketing ] Facebook Marketing Pixel

</details>

***
<br>

## Testing your setup

### 1. OneTrust is implemented

To test is OneTrust is implemented take these steps:  
1. Go to the campaign page / location [Lipton Example](https://unilever.qualifioapp.com/quiz/904456_2397/Lipton-Terrace-Quiz.html)
2. If you see the cookiesign on the left of the screen, Onetrust is available

*Visual Details*  

<details>
<br>

*Cookie consent being available (visual mark)*  
![visual cookieconsent](/images/testing/cookieconsent_implemented.png "visual cookieconsent")

*Cookie consent being available (through the browsers network tab // F12 >> Network)*  
![cookieconsent in network](/images/testing/onetrust_in_network_tab.png "cookieconsent in network")

</details>

***

### 2. Google Tag Manager is implemented
Testing if the Tag Manager is available requires these 2 steps:
1. Go to the campaign page / location [Lipton Example](https://unilever.qualifioapp.com/quiz/904456_2397/Lipton-Terrace-Quiz.html)
2. Use your browsers development mode (through the browsers network tab // F12 >> Network)
3. Look for gtm (if not available reload the page while having the development tools open)

<details>
<br>

*Through a visual OmniBug Extension (Chrome / Edge)*  
![shown through omnibug](/images/testing/gtm_in_omnibug.png "shown through omnibug")

*Google tag manager being available (through the browsers network tab // F12 >> Network)*
![shown through network](/images/testing/gtm_in_network.png "shown through network")  

</details>

***

### 3. Is a dataLayer available
Your next step should be to verify if dataLayer is available through these steps:
1. Go to the campaign page / location [Lipton Example](https://unilever.qualifioapp.com/quiz/904456_2397/Lipton-Terrace-Quiz.html)
2. Use your browsers development mode (through the browsers network tab // F12 >> Console)
3. use the command **dataLayer** and check if these items are available:
   1. OneTrustLoaded ( => accept optin & load again if not visible )
   2. OptanonLoaded ( => accept optin & load again if not visible )
   3. OneTrustGroupsUpdated ( => accept optin & load again if not visible )
   4. event => virtualPageview

<details>
<br>

*Scripts / tags are loading*  
![dataLayer testing](/images/testing/datalayer_testing.png "dataLayer testing") 

</details>

*** 

### 3. Scripts are loading
To check if scripts are loadig you can take these steps:
1. Go to the campaign page / location [Lipton Example](https://unilever.qualifioapp.com/quiz/904456_2397/Lipton-Terrace-Quiz.html)
2. Use your browsers development mode (through the browsers network tab // F12 >> Omnibug >> for installing the extension) >> 
   1. https://chrome.google.com/webstore/detail/omnibug/bknpehncffejahipecakbfkomebjmokl
3. Load the page & look for tags like google analytics / facebook

<details>
<br>

*Scripts / tags are loading*  
![tags in omnibug](/images/testing/scripts_tags_in_omnibug.png "tags in omnibug") 

</details>

***