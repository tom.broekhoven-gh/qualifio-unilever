/**
* Qualifio Tagging plan 
* >> don't forget to wrap this those in <script tags>
* 
* code is build up in 3 parts
* 1. initial layer of data
* 2. pushing a virtual page
* 3. pushing other events
* 
* >> items that need setting
*  _brandName => brand used in campaign
*  _ulCampaignId => campaign name like PN<other characters>
* 
* >> parameters that are supported
*  campaign_brand => brand used in campaign (input based on _brandName)
*  unilever_campaign_id => campaign (input based on _ulCampaignId)
*  page_path => path of the page /example/example 
*  title => page title
*  campaign_id => qualifio campaign id
*  campaign_guid => qualifio campaign guid
*  channel_guid => qualifio channel id
*  device => mobile / desktop etc.
*  language => user system language
*  page => page name by qualifio
*  step => step name by qualifio
*  pgi => page index number (like 1,2,3)
*  domain => domain of the page
*  winner => is user is a winner (TRUE or FALSE)
*  score => score, if a score is available
*  location => full document location
*  utm_parameters => campaign parameters
*  event => name of the event used in tag manager references
* 
* >> supported events
*  virtualPageview => used as soon as a virtual page is triggered
*  gaEvent => all other type of events being used:
*      
*/
$(function() {
    
    /**
    * set qualifio generic script
    * push to the parent page
    */
    
    var  tp = _qlf_taggingplan,
    _brandName = '',        // fill in the brand name or abreviation
    _ulCampaignId = '';     // fill in the campaign id
    
    /* get qualifio datalayer */
    qlfd = qlfDataLayer[0];
    
    /* set dataLayer object */
    dataLayerPageView = {
        'campaign_brand': _brandName,
        'unilever_campaign_id': _ulCampaignId,
        'page_path': qlfd['page_path'] + '?{utms}',
        'title': qlfd['title'],
        'campaign_id': qlfd['campaign_id'],
        'campaign_guid': qlfd['campaign_guid'],
        'channel_guid': qlfd['channel_guid'],
        'device': qlfd['device'],
        'language': qlfd['lg'],
        'page': qlfd['pg'],
        'step': qlfd['step'],
        'pgi': qlfd['pgi'],
        'domain': qlfd['domain'],
        'winner': qlfd['isWinner'],
        'score': qlfd['score'],
        'location': qlfd['parentURL'] + qlfd['page_path'] + '?{utms}',
        'utm_parameters': '{utms}',
        'event': 'virtualPageview'
    };
    
    /*********************************************************************** */
    /* IMPORTANT: _qual_async is only to be used in standalone */
    /*********************************************************************** */
    
    /**
    * Virtual Pageview
    */
    _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
        _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', dataLayerPageView]);
    }]);
    
    /** Scores*/
    $('#s_scores').click(function(){
        _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
            _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                'event':'gaEvent',
                'eventCategory':'Click',
                'eventAction': 'Rules',
                'eventLabel': qlfDataLayer['0']['title']
            }]);
        }]);    
    });
    
    /** Rules */
    $('#s_reglement').click(function(){
        _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
            _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                'event':'gaEvent',
                'eventCategory':'Click',
                'eventAction': 'Scores',
                'eventLabel': qlfDataLayer['0']['title']
            }]);
        }]);    
    });
    
    
    /**
    * Play the game
    */
    if (tp.getStepName() == 'intro') {
        // Play button
        $('#jouer').click(function(){
            _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                    'event':'gaEvent',
                    'eventCategory':'Click',
                    'eventAction': 'Participate',
                    'eventLabel': qlfDataLayer['0']['title']
                }]);
            }]);    
        });
    }
    /**
    * Question set
    */
    if (tp.getStepName().startsWith("questionset")) {
        /** next */
        $('#suivant1, #next').click(function(){
            _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                    'event':'gaEvent',
                    'eventCategory':'Question ' + qlfDataLayer['0']['pgi'],
                    'eventAction': 'Answered',
                    'eventLabel': qlfDataLayer['0']['title']
                }]);
            }]); 
            
        });
        /** Images */
        $('.answerPic').click(function() {
            _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                    'event':'gaEvent',
                    'eventCategory':'Question ' + qlfDataLayer['0']['pgi'],
                    'eventAction': 'Answered',
                    'eventLabel': qlfDataLayer['0']['title']
                }]);
            }]); 
        });
        /** Answering */
        $('[id^=check_choix]').click(function() {
            _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                    'event':'gaEvent',
                    'eventCategory':'Question ' + qlfDataLayer['0']['pgi'],
                    'eventAction': 'Answered',
                    'eventLabel': qlfDataLayer['0']['title']
                    
                }]);
            }]); 
        });
    }
    if (tp.isFormSubmittedRightNow()) { 
        /** form submit data */    
        _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
            _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                'event':'gaEvent',
                'eventCategory':'Form',
                'eventAction': 'Submit',
                'eventLabel': qlfDataLayer['0']['title']
            }]);
        }]);
    }
    if (tp.getStepName().startsWith("exit")) { 
        /** winner data */
        if (tp.isWinner() == 1) { 
            _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                    'event': 'gaEvent',
                    'eventCategory': 'Click',
                    'eventAction': 'Quiz finished - Won',
                    'eventLabel': qlfDataLayer['0']['title']
                }]);
            }]);   
        }
        if (tp.isWinner() == 0) { 
            _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                    'event': 'gaEvent',
                    'eventCategory': 'Click',
                    'eventAction': 'Quiz finished - Lost',
                    'eventLabel': qlfDataLayer['0']['title']
                }]);
            }]);  
        }
    }
    /** share facebookj */
    $('#fbShareWall a').click(function() {
        if (tp.isAShareFor(this, "facebook")) {
            _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                    'event': 'gaEvent',
                    'eventCategory': 'Click',
                    'eventAction': 'Share participation - Facebook',
                    'eventLabel': qlfDataLayer['0']['title']
                }]);
            }]);  
            
        }
    });
    /** share twitter */
    $('#fbShareWall a').click(function() {
        if (tp.isAShareFor(this, "twitter")) {
            _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
                _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                    'event': 'gaEvent',
                    'eventCategory': 'Click',
                    'eventAction': 'Share participation - Twitter',
                    'eventLabel': qlfDataLayer['0']['title']
                }]);
            }]);  
            
        }
    });
    /** replay the game */
    $('#fbShareWallBonus > a.bouton').click(function() {
        _qual_async.push(['requestPlugin', 'qualp', 'datalayer', function() {
            _qual_async.push(['sendParentMessage', 'datalayer/dataLayerPush', {
                'event': 'gaEvent',
                'eventCategory': 'Click',
                'eventAction': 'Replay',
            }]);
        }]);  
    });
    
    
    /*********************************************************************** */
    /* IMPORTANT: dataLayer.push is only to be used in iframed situation */
    /*********************************************************************** */
    
    /**
    * Virtual Pageview
    */
    dataLayer.push(dataLayerPageView);
    
    /** Scores*/
    $('#s_scores').click(function(){
        dataLayer.push({
            'event':'gaEvent',
            'eventCategory':'Click',
            'eventAction': 'Rules',
            'eventLabel': qlfDataLayer['0']['title']
        });  
    });
    
    /** Rules */
    $('#s_reglement').click(function(){
        _dataLayer.push({
            'event':'gaEvent',
            'eventCategory':'Click',
            'eventAction': 'Scores',
            'eventLabel': qlfDataLayer['0']['title']
        });
        
    });
    
    
    /**
    * Play the game
    */
    if (tp.getStepName() == 'intro') {
        // Play button
        $('#jouer').click(function(){
            dataLayer.push({
                'event':'gaEvent',
                'eventCategory':'Click',
                'eventAction': 'Participate',
                'eventLabel': qlfDataLayer['0']['title']
                
            });    
        });
    }
    /**
    * Question set
    */
    if (tp.getStepName().startsWith("questionset")) {
        /** next */
        $('#suivant1, #next').click(function(){
            dataLayer.push({
                'event':'gaEvent',
                'eventCategory':'Question ' + qlfDataLayer['0']['pgi'],
                'eventAction': 'Answered',
                'eventLabel': qlfDataLayer['0']['title']
                
            }); 
            
        });
        /** Images */
        $('.answerPic').click(function() {
            dataLayer.push({
                'event':'gaEvent',
                'eventCategory':'Question ' + qlfDataLayer['0']['pgi'],
                'eventAction': 'Answered',
                'eventLabel': qlfDataLayer['0']['title']
            }); 
        });
        /** Answering */
        $('[id^=check_choix]').click(function() {
            dataLayer.push({
                'event':'gaEvent',
                'eventCategory':'Question ' + qlfDataLayer['0']['pgi'],
                'eventAction': 'Answered',
                'eventLabel': qlfDataLayer['0']['title']
                
            });
        });
    }
    if (tp.isFormSubmittedRightNow()) { 
        /** form submit data */    
        _dataLayer.push({
            'event':'gaEvent',
            'eventCategory':'Form',
            'eventAction': 'Submit',
            'eventLabel': qlfDataLayer['0']['title']
        });
    }
    if (tp.getStepName().startsWith("exit")) { 
        /** winner data */
        if (tp.isWinner() == 1) { 
            dataLayer.push({
                'event': 'gaEvent',
                'eventCategory': 'Click',
                'eventAction': 'Quiz finished - Won',
                'eventLabel': qlfDataLayer['0']['title']
            });  
        }
        if (tp.isWinner() == 0) { 
            dataLayer.push({
                'event': 'gaEvent',
                'eventCategory': 'Click',
                'eventAction': 'Quiz finished - Lost',
                'eventLabel': qlfDataLayer['0']['title']
            });
        }
    }
    /** share facebookj */
    $('#fbShareWall a').click(function() {
        if (tp.isAShareFor(this, "facebook")) {
            dataLayer.push({
                'event': 'gaEvent',
                'eventCategory': 'Click',
                'eventAction': 'Share participation - Facebook',
                'eventLabel': qlfDataLayer['0']['title']
            });  
            
        }
    });
    /** share twitter */
    $('#fbShareWall a').click(function() {
        if (tp.isAShareFor(this, "twitter")) {
            dataLayer.push({
                'event': 'gaEvent',
                'eventCategory': 'Click',
                'eventAction': 'Share participation - Twitter',
                'eventLabel': qlfDataLayer['0']['title']
            }); 
        }
    });
    /** replay the game */
    $('#fbShareWallBonus > a.bouton').click(function() {
        dataLayer.push({
            'event': 'gaEvent',
            'eventCategory': 'Click',
            'eventAction': 'Replay'
        }); 
    });
});